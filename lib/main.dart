import 'package:flutter/material.dart';
import 'common.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Tábla',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Tábla'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var resp = "";

  void tryAuth() {
    auth(school, user, password);
    setState(() {
      resp = "Auth'd";
    });
  }
  void time() {
    getTime(school).then((response) {
      setState(() {
        resp = response.body;
      });
    });
  }
  void orarend() {
    getOrarend(school, "").then((response) {
      setState(() {
        resp = response.body;
      });
    });
  }
  void fali() {
    getFaliujs(school, "").then((response) {
      setState(() {
        print(response.body);
        resp = response.body;
      });
    });
  }
  void clear(){
    setState(() {
      resp = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Center(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new RaisedButton(
                    onPressed: tryAuth,
                    child: new Text("Auth"),
                  ),
                  new RaisedButton(
                    onPressed: time,
                    child: new Text("Time"),
                  ),
                  new RaisedButton(
                    onPressed: orarend,
                    child: new Text("Órarend"),
                  ),
                  new RaisedButton(
                    onPressed: fali,
                    child: new Text("Faliújság"),
                  ),
                ],
              ),
            ),
            new Text(
              'Válasz a Krétától:',
            ),
            new Text(
              '$resp',
            ),
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: clear,
        tooltip: 'Increment',
        child: new Icon(Icons.add),
      ),
    );
  }
}
