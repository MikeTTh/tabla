import 'dart:async';
import 'dart:io';

import 'package:http/http.dart' as http;

var school = "klik037634001";
var user = "73389931801";
var password = "jelszojelszo";
Map<String, String> cookies = {};
String CSRFToken = "";

Map<String, String> getHeaders(){
  Map<String, String> tmp = {};
  tmp.addAll({'Cookie': cookiesToStr(), 'X-Request-Verification-Token': CSRFToken});
  tmp.addAll({
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'X-Requested-With': 'XMLHttpRequest',
    'Referer': 'https://klik037634001.e-kreta.hu/Orarend/InformaciokOrarend',
  });
  return tmp;
}

String cookiesToStr(){
  String tmp = "";
  if (cookies.length > 0){
    cookies.forEach((k, v){
      tmp += "$k=$v; ";
    });
    //tmp = tmp.substring(0, tmp.length-1);
  }
  return tmp;
}

void strToCookies(String input) {
  input.split(",").forEach((c){
    var k = c.split(";")[0].split("=")[0];
    var v = c.split(";")[0].split("=")[1];
    cookies[k] = v;
  });
}

void auth(String school, user, password) {
  cookies = {};
  http.post("https://$school.e-kreta.hu/Adminisztracio/Login/LoginCheck", body: {"UserName": "$user", "Password": "$password"}).then((resp){
    if (resp.headers.containsKey("set-cookie")) {
      strToCookies(resp.headers["set-cookie"]);
    }
    http.get("https://$school.e-kreta.hu/Adminisztracio/SzerepkorValaszto", headers: {'Cookie': cookiesToStr()}).then((resp){
      if (resp.headers.containsKey("set-cookie")) {
        strToCookies(resp.headers["set-cookie"]);
      }
      http.get("https://$school.e-kreta.hu/Orarend/InformaciokOrarend", headers: {'Cookie': cookiesToStr()}).then((resp){
        if (resp.headers.containsKey("set-cookie")) {
          strToCookies(resp.headers["set-cookie"]);
        }
        CSRFToken = resp.body.split("__RequestVerificationToken")[1].split("value=\"")[1].split("\"")[0];
        print(cookiesToStr());
        print(CSRFToken);
      });
    });
  });
}

Future<http.Response> getOrarend(String school, week){
  return http.get("https://$school.e-kreta.hu/api/CalendarApi/GetTanuloOrarend?tanarId=-1&osztalyCsoportId=-1&tanuloId=-1&teremId=-1&kellCsengetesiRendMegjelenites=false&csakOrarendiOra=false&kellTanoranKivuliFoglalkozasok=false&kellTevekenysegek=false&kellTanevRendje=true&szuresTanevRendjeAlapjan=false&start=2018-09-03&end=2018-09-08&_=1535915584845", headers: getHeaders());
}
Future<http.Response> getFaliujs(String school, week){
  return http.get("https://$school.e-kreta.hu/Intezmeny/Faliujsag/GetMoreEntries?startindex=0&range=10&_=153", headers: getHeaders());
}
Future<http.Response> getTime(String school){
  return http.get("https://$school.e-kreta.hu/Layout/GetRemainingTime", headers: getHeaders());
}